import { Component, OnInit, ChangeDetectionStrategy } from "@angular/core";
import { EMPTY, Observable, Subject } from "rxjs";
import { Product } from "../product";
import { ProductService } from "../product.service";
import { catchError } from "rxjs/operators";

@Component({
  selector: "pm-product-list",
  templateUrl: "./product-list-alt.component.html",
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductListAltComponent implements OnInit {
  pageTitle = "Products";
  private errorMessageSubject = new Subject<string>();
  errorMessage$ = this.errorMessageSubject.asObservable();

  products$: Observable<Product[]> = this.productService.products$.pipe(
    catchError((err) => {
      this.errorMessageSubject.next(err);
      return EMPTY;
    })
  );

  selectedProduct$ = this.productService.selectedProduct$;

  constructor(private productService: ProductService) {}

  ngOnInit(): void {}

  onSelected(productId: number): void {
    this.productService.selectedProductChange(productId);
  }
}

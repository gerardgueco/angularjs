import { Component } from "@angular/core";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.css"]
})
export class UserComponent {
  // Properties
  firstName: string;
  lastName: string;
  age: number;
  address;

  hasKids: boolean;
  numberArray: number[];
  stringArray: string[];
  mixedArray: any[];
  myTuple: [string, number, boolean];

  // Methods
  constructor() {
    this.firstName = "John";
    this.lastName = "Smith";
    this.age = 30;
    this.hasKids = true;
    this.numberArray = [1, 2, 3];
    this.stringArray = ["hello", "world"];
    this.mixedArray = ["hello", undefined, true];
    this.myTuple = ["hello", 1, false];

    this.address = {
      street: "50 Main Street",
      city: "Boston",
      state: "MA"
    };
  }
}

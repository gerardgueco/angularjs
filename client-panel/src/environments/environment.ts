// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAIBcQAvKmzAA80yvHoGHT3M5Q662f0s4c",
    authDomain: "client-panel-d2c75.firebaseapp.com",
    databaseURL: "https://client-panel-d2c75.firebaseio.com",
    projectId: "client-panel-d2c75",
    storageBucket: "client-panel-d2c75.appspot.com",
    messagingSenderId: "802859172571",
    appId: "1:802859172571:web:bba9a3d17f8539eb1ec2fe"
  }
};

import { Component, OnInit } from "@angular/core";
import { interval, Subscription, Observable } from "rxjs";
import { map, filter } from "rxjs/operators";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  private firstObsSubscribtion: Subscription;

  constructor() {}

  ngOnInit() {
    // this.firstObsSubscribtion = interval(1000).subscribe(count => {
    //   console.log(count);
    // });
    const customIntervalObservable = Observable.create(observer => {
      let count = 0;
      setInterval(() => {
        observer.next(count);
        count++;
      }, 1000);
    });

    this.firstObsSubscribtion = customIntervalObservable
      .pipe(
        filter((data: number) => {
          return data > 0;
        }),
        map((data: number) => {
          return "Round: " + (data + 1);
        })
      )
      .subscribe(count => {
        console.log(count);
      });

    // this.firstObsSubscribtion = customIntervalObservable.subscribe(count => {
    //   console.log(count);
    // });
  }

  ngOnDestroy(): void {
    this.firstObsSubscribtion.unsubscribe();
  }
}

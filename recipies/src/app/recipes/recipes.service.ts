import { Injectable, EventEmitter } from "@angular/core";
import { Recipe } from "./recipe.model";
import { Ingredient } from "../shared/ingredient.model";
import { ShopingListService } from "../shoping-list/shoping-list.service";

@Injectable({
  providedIn: "root"
})
export class RecipesService {
  private recipes: Recipe[] = [
    new Recipe(
      "Hamburger",
      "A hamburger (also burger for short) is a food consisting of one or more cooked patties of ground meat, usually beef, placed inside a sliced bread roll or bun.",
      "https://img.buzzfeed.com/thumbnailer-prod-us-east-1/video-api/assets/165384.jpg",
      [
        new Ingredient("Meat", 1),
        new Ingredient("Buns", 1),
        new Ingredient("Tomato", 1),
        new Ingredient("Onions", 1)
      ]
    ),
    new Recipe(
      "Pad Thai",
      "Pad Thai is a Thai noodle stir fry with a sweet-savoury-sour sauce scattered with crushed peanuts. It’s made with thin, flat rice noodles, and almost always has bean sprouts, garlic chives, scrambled egg, firm tofu and a protein – the most popular being chicken or prawns/shrimp.",
      "https://www.recipetineats.com/wp-content/uploads/2018/05/Chicken-Pad-Thai_2-650x910.jpg",
      [
        new Ingredient("Noodles", 1),
        new Ingredient("Egg", 1),
        new Ingredient("Onions", 1)
      ]
    )
  ];

  recipeSelected = new EventEmitter<any>();

  constructor(private shoppingListService: ShopingListService) {}

  getRecipes() {
    return this.recipes.slice();
  }

  getRecipe(id: number): Recipe {
    return this.recipes[id];
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.shoppingListService.addIngredients(ingredients);
  }
}

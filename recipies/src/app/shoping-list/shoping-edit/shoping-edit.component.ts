import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  Output,
  EventEmitter
} from "@angular/core";

import { Ingredient } from "../../shared/ingredient.model";
import { ShopingListService } from "../shoping-list.service";

@Component({
  selector: "app-shoping-edit",
  templateUrl: "./shoping-edit.component.html",
  styleUrls: ["./shoping-edit.component.css"]
})
export class ShopingEditComponent implements OnInit {
  @ViewChild("nameInput", { static: false }) nameInputRef: ElementRef;
  @ViewChild("amountInput", { static: false }) amountInputRef: ElementRef;
  @Output() ingredientAdded = new EventEmitter<Ingredient>();

  constructor(private shopingListService: ShopingListService) {}

  ngOnInit(): void {}

  onAdd() {
    const newIngredient = new Ingredient(
      this.nameInputRef.nativeElement.value,
      this.amountInputRef.nativeElement.value
    );

    // this.ingredientAdded.emit(newIngredient);

    this.shopingListService.addIngredient(newIngredient);
  }
}

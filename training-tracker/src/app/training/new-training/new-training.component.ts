import { Component, OnInit, OnDestroy } from "@angular/core";
import { Excercise } from "../excercise.model";
import { TrainingService } from "../training.service";
import { NgForm } from "@angular/forms";
import { Subscription } from "rxjs";
import { UIService } from "src/app/shared/ui.service";

@Component({
  selector: "app-new-training",
  templateUrl: "./new-training.component.html",
  styleUrls: ["./new-training.component.scss"],
})
export class NewTrainingComponent implements OnInit {
  excercises: Excercise[];
  excercisesSubscribtion: Subscription;
  loadingSubscribtion: Subscription;
  isLoading = false;
  constructor(
    private trainingService: TrainingService,
    private uiService: UIService
  ) {}

  ngOnInit(): void {
    this.loadingSubscribtion = this.uiService.loadingStateChanged.subscribe(
      (isLoading) => {
        this.isLoading = isLoading;
      }
    );
    this.excercisesSubscribtion = this.trainingService.excercisesChanged.subscribe(
      (data: Excercise[]) => {
        this.excercises = data;
      }
    );
    this.fetchExcercises();
  }

  ngOnDestroy() {
    this.loadingSubscribtion.unsubscribe();
    this.excercisesSubscribtion.unsubscribe();
  }

  onStartTraining(form: NgForm) {
    this.trainingService.startExcercise(form.value.excercise);
  }

  fetchExcercises() {
    this.trainingService.fetchAvalableExcercises();
  }
}

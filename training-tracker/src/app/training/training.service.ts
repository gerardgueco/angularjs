import { Injectable } from "@angular/core";
import { Subject, Observable, Subscription } from "rxjs";
import { map } from "rxjs/operators";
import { AngularFirestore } from "@angular/fire/firestore";
import { Excercise } from "./excercise.model";
import { UIService } from "../shared/ui.service";

@Injectable()
export class TrainingService {
  excerciseChanged = new Subject<Excercise>();
  excercisesChanged = new Subject<Excercise[]>();
  finishedExcercises = new Subject<Excercise[]>();

  private availableExcercises: Excercise[] = [];
  private runningExcercise: Excercise;
  private firestoreSubs: Subscription[] = [];

  constructor(
    private firestore: AngularFirestore,
    private uiService: UIService
  ) {}

  fetchAvalableExcercises() {
    this.firestoreSubs.push(
      this.firestore
        .collection("availableExcercises")
        .snapshotChanges()
        .pipe(
          map((response) => {
            return response.map((doc) => {
              const data: any = doc.payload.doc.data();
              return { id: doc.payload.doc.id, ...data };
            });
          })
        )
        .subscribe(
          (data: Excercise[]) => {
            this.availableExcercises = data;
            this.excercisesChanged.next([...this.availableExcercises]);
          },
          (error) => {
            this.uiService.loadingStateChanged.next(false);
            this.uiService.showSnackbar(
              "Fetching exercises failed. Try again later!"
            );
            this.excercisesChanged.next([null]);
          }
        )
    );
  }

  startExcercise(selectedId: string) {
    // Update doc
    // this.firestore
    //   .doc("availableExcercises/" + selectedId)
    //   .update({ lastSelected: new Date() });
    this.runningExcercise = this.availableExcercises.find(
      (excercise) => excercise.id === selectedId
    );
    this.excerciseChanged.next({ ...this.runningExcercise });
  }

  getRunningExcercise() {
    return { ...this.runningExcercise };
  }

  completeExcercise() {
    this.addDataToFirestore({
      ...this.runningExcercise,
      date: new Date(),
      state: "completed",
    });
    this.runningExcercise = null;
    this.excerciseChanged.next(null);
  }

  cancelExcercise(progress: number) {
    this.addDataToFirestore({
      ...this.runningExcercise,
      duration: this.runningExcercise.duration * (progress / 100),
      calories: this.runningExcercise.calories * (progress / 100),
      date: new Date(),
      state: "cancelled",
    });
    this.runningExcercise = null;
    this.excerciseChanged.next(null);
  }

  fethcAllExcercises() {
    this.firestoreSubs.push(
      this.firestore
        .collection("finishedExcercises")
        .valueChanges()
        .subscribe(
          (excersises: Excercise[]) => {
            this.finishedExcercises.next(excersises);
          },
          (error) => {
            // this.uiService.showSnackbar(error.message);
          }
        )
    );
  }

  cancelSubscribtions() {
    this.firestoreSubs.forEach((sub) => sub.unsubscribe());
  }

  private addDataToFirestore(excercise: Excercise) {
    this.firestore.collection("finishedExcercises").add(excercise);
  }
}
